import pytest
import json
from fastapi.testclient import TestClient
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from sink.main import app
from sink.lbsn import sinkdb


client = TestClient(app)


def test_get_rules():

    response = client.get("/rules")
    assert response.status_code == 200


@pytest.mark.order(1)
def test_create_rule(example_rule, example_post, example_tweet):

    post_response = client.post("/rules", json=example_rule)

    # remember rule_id for future tests
    example_rule["id"] = json.loads(post_response.text)["data"][0]["id"]
    example_post["rules"].append(example_rule["id"])
    example_tweet["matching_rules"][0]["id"] = example_rule["id"]

    assert post_response.status_code == 201
    assert post_response.json()["meta"]["summary"]["created"] == 1


@pytest.mark.order(2)
def test_rule_table_is_created(example_rule):

    table_name = sinkdb.prefix + example_rule["id"]

    sinkdb_connection = sinkdb.connect()
    sinkdb_cursor = sinkdb_connection.cursor()
    query = f"""
        SELECT EXISTS (
            SELECT 1
            FROM   information_schema.tables
            WHERE  table_schema = '{sinkdb.schema}'
            AND    table_name = '{table_name}'
        );
    """

    sinkdb_cursor.execute(query)
    assert sinkdb_cursor.fetchone()[0] is True
    sinkdb_cursor.close()
    sinkdb_connection.close()


@pytest.mark.order(3)
def test_recreate_rule(example_rule):

    repost_response = client.post("/rules", json=example_rule)

    assert repost_response.status_code == 409
    assert repost_response.json()["meta"]["summary"]["not_created"] == 1


@pytest.mark.order(4)
def test_post_twitter_post(example_tweet):

    post_response = client.post("/posts", json=example_tweet)
    assert post_response.status_code == 201


@pytest.mark.order(5)
def test_post_is_stored(example_rule):

    table_name = sinkdb.schefix + example_rule["id"]

    sinkdb_connection = sinkdb.connect()
    sinkdb_cursor = sinkdb_connection.cursor()
    query = f"""
        SELECT hll_cardinality(id)
        FROM {table_name};
    """
    sinkdb_cursor.execute(query)
    assert sinkdb_cursor.fetchone()[0] == 1
    sinkdb_cursor.close()
    sinkdb_connection.close()


@pytest.mark.order(6)
def test_cardinality(example_tweet):

    rule_id = example_tweet["matching_rules"][0]["id"]

    get_response = client.get(f"/rules/{rule_id}")
    json = get_response.json()

    assert get_response.status_code == 200
    assert json["metadata"]["id"] == rule_id
    assert json["features"][0]["properties"]["cardinality"] == 1

    point = Point(example_tweet["data"]["geo"]["coordinates"]["coordinates"])
    area = Polygon(json["features"][0]["geometry"]["coordinates"][0])

    assert area.contains(point)


@pytest.mark.order(7)
def test_delete_rule(example_rule):

    rule_id = example_rule["id"]

    delete_response = client.delete(f"/rules/{rule_id}")

    assert delete_response.status_code == 200
    assert delete_response.json()["meta"]["summary"]["deleted"] == 1


@pytest.mark.order(8)
def test_rule_table_is_deleted(example_rule):

    table_name = sinkdb.prefix + example_rule["id"]

    sinkdb_connection = sinkdb.connect()
    sinkdb_cursor = sinkdb_connection.cursor()
    query = f"""
        SELECT EXISTS (
            SELECT 1
            FROM   information_schema.tables
            WHERE  table_schema = '{sinkdb.schema}'
            AND    table_name = '{table_name}'
        );
    """

    sinkdb_cursor.execute(query)
    assert sinkdb_cursor.fetchone()[0] is False
    sinkdb_cursor.close()
    sinkdb_connection.close()
