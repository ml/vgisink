import pytest
import time


@pytest.fixture(scope="module")
def example_rule():
    "unix time stamp for pseudo-random content"
    return {
        "id": "1435230301286961154",
        "value": "integration test at " + str(int(time.time())),
        "precision": "3",
        "tag": "ci_test",
    }


@pytest.fixture(scope="module")
def example_post(example_rule):
    return {
        "id": str(int(time.time())),
        "rules": [1444991881012826116],
        "coordinates": {
            "lat": 47.109375,
            "lon": -117.421875,
        },
    }


@pytest.fixture(scope="module")
def abstracted_post():
    return {
        "id": "1435230301286961154",
        "rules": [1444991881012826116],
        "coordinates": (47.109375, -117.421875),
    }


@pytest.fixture(scope="module")
def example_tweet():

    return {
        "data": {
            "author_id": "930739604",
            "created_at": "2021-09-07T13:15:43.000Z",
            "geo": {
                "coordinates": {
                    "type": "Point",
                    "coordinates": [-117.3684, 47.61353],
                },
                "place_id": "dc3747428fa88cab",
            },
            "id": "1435230301286961154",
            "text": "Grass fire in #MoranPrairie on E 44th Ave Both EB/WB",
        },
        "includes": {
            "places": [
                {
                    "full_name": "Spokane, WA",
                    "geo": {
                        "type": "Feature",
                        "bbox": [-117.565226, 47.5742, -117.303868, 47.760676],
                        "properties": {},
                    },
                    "id": "dc3747428fa88cab",
                    "name": "Spokane",
                }
            ]
        },
        "matching_rules": [
            {"id": 1444991881012826116, "tag": "disaster fire"}
        ],
    }
