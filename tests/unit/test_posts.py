def test_sort_post_coordinates(abstracted_post):

    from sink.lbsn.posts import sort_post_coordinates

    sorted_post = sort_post_coordinates(abstracted_post)

    assert sorted_post["id"] == "1435230301286961154"
    assert sorted_post["rules"] == [1444991881012826116]
    assert sorted_post["coordinates"]["lat"] == 47.109375
    assert sorted_post["coordinates"]["lon"] == -117.421875
