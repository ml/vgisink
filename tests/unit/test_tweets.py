def test_filter_tweet(example_tweet):

    from sink.twitter.tweets import filter_tweet

    filtered_tweet = filter_tweet(example_tweet)

    assert filtered_tweet["id"] == "1435230301286961154"
    assert filtered_tweet["rules"] == [1444991881012826116]
    assert filtered_tweet["coordinates"] == [-117.3684, 47.61353]
    assert "data" not in filtered_tweet
    assert "includes" not in filtered_tweet
    assert "matching_rules" not in filtered_tweet
