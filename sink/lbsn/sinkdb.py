import os
import psycopg2

schema = "rules"
prefix = "rule_"

# schema and prefix
schefix = schema + "." + prefix


def connect():
    "Return a connection to sinkDB"
    connection = psycopg2.connect(
        dbname=os.environ.get("SINKDB_NAME"),
        host=os.environ.get("SINKDB_HOST"),
        port=os.environ.get("SINKDB_PORT"),
        user=os.environ.get("SINKDB_USER"),
        password=os.environ.get("SINKDB_PASS"),
    )
    return connection
