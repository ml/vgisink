from sink.lbsn import sinkdb
from sink.twitter.rules import get_rules


def get_cardinalities(rule_id):
    """
    For a given rule ID, return a GeoJSON object with polygons
    representing the stored geohashes plus their respective cardinality
    """

    # open database connection
    sinkdb_connection = sinkdb.connect()
    sinkdb_cursor = sinkdb_connection.cursor()

    # assemble database query
    table_name = sinkdb.schefix + str(rule_id)
    query = f"""
        SELECT json_build_object(
            'type', 'Feature',
            'geometry', ST_AsGeoJSON(polygon)::jsonb,
            'properties', json_build_object('cardinality',cardinality)
        ) FROM (
            SELECT hll_cardinality(id) AS cardinality,
                ST_ForcePolygonCCW(ST_GeomFromGeoHash(geohash)) AS polygon
            FROM {table_name}
        ) AS features
        """

    # execute query
    sinkdb_cursor.execute(query)

    # receive query response
    db_response = sinkdb_cursor.fetchall()

    # close database connection
    sinkdb_cursor.close()
    sinkdb_connection.close()

    # get rule metadata from twitter
    rules = get_rules().json()
    for rule in rules["data"]:
        if rule["id"] == rule_id:
            # assemble response object
            response = {
                "type": "FeatureCollection",
                "features": [],
                "metadata": rule,
            }

    # add every feature to response features dict
    for feature in db_response:
        response["features"].append(feature[0])

    return response
