from sink.lbsn import sinkdb


def sort_post_coordinates(post):
    """
    Create unambiguous keys for latitude and longitude
    in the correct order according to ISO 6709
    """

    post["coordinates"] = {
        "lat": post["coordinates"][0],
        "lon": post["coordinates"][1],
    }

    return post


def store_post(post):
    """
    Create the database connection. Assemble and runs a query to store the
    post in the appropriate database table:

    * select the geohash precision value from the information_schema column
      field definition `character_maximum_length`
    * compute a geohash from the coordinates using Postgis functions
    * if this is the first post at that geohash, create a new HLL set by
      generating the hash of the post id using `hll_hash_bigint()` and then
      adding it to an empty set using the hll_add() function.
    * else, if this is not the first post at that geohash (`CONFLICT`), use
      the just calculated HLL set (`EXCLUDED.id`) and unify it with the already
      existing HLL set (`{table_name}.id`) using `hll_union()`.
    """

    sinkdb_connection = sinkdb.connect()
    sinkdb_cursor = sinkdb_connection.cursor()

    for rule_id in post["rules"]:

        schema_and_table_name = sinkdb.schefix + str(rule_id)
        table_name = sinkdb.prefix + str(rule_id)

        query = f"""
            INSERT INTO {schema_and_table_name} (geohash, id)
            VALUES (
                ST_GeoHash(
                    ST_SetSRID(
                        ST_Point(
                            {post["coordinates"]["lat"]},
                            {post["coordinates"]["lon"]}
                        ),
                        4326
                    ),(
                        SELECT character_maximum_length
                        FROM information_schema.columns
                        WHERE column_name = 'geohash'
                        AND table_name = '{table_name}'
                    )
                ),
                hll_add(hll_empty(), hll_hash_bigint({post["id"]}))
            )
            ON CONFLICT (geohash)
            DO UPDATE SET id = hll_union(
                EXCLUDED.id,
                {table_name}.id
            )
            """

        try:
            sinkdb_cursor.execute(query)
            sinkdb_connection.commit()
            stored = True

        except Exception:
            sinkdb_connection.rollback()
            stored = False

    sinkdb_cursor.close()
    sinkdb_connection.close()

    return stored
