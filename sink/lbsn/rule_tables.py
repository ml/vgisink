from sink.lbsn import sinkdb


def create_rule_table(rule_id, rule_value, rule_precision):
    """
    Create a table for this rule named by the passed rule_id.
    Return false in case of an exception, otherwise return true.
    """

    sinkdb_connection = sinkdb.connect()
    sinkdb_cursor = sinkdb_connection.cursor()

    table_name = sinkdb.schefix + rule_id

    query = f"""
        CREATE TABLE IF NOT EXISTS {table_name} (
            geohash varchar({rule_precision}),
            PRIMARY KEY (geohash),
            id hll
        );
        COMMENT ON TABLE {table_name} IS '{rule_value}';
    """

    try:
        sinkdb_cursor.execute(query)
        sinkdb_connection.commit()
        created = True

    except Exception:
        sinkdb_connection.rollback()
        created = False

    sinkdb_cursor.close()
    sinkdb_connection.close()

    return created


def drop_rule_table(rule_id):
    """
    Delete a table according to the given rule_id
    """

    sinkdb_connection = sinkdb.connect()
    sinkdb_cursor = sinkdb_connection.cursor()

    table_name = sinkdb.schefix + rule_id
    query = f"DROP TABLE IF EXISTS {table_name};"

    try:
        sinkdb_cursor.execute(query)
        sinkdb_connection.commit()
        dropped = True
    except Exception:
        sinkdb_connection.rollback()
        dropped = False

    sinkdb_cursor.close()
    sinkdb_connection.close()

    return dropped
