from shapely.geometry import MultiPoint


def filter_tweet(tweet):
    """
    Filter tweet dictionary for the required data subset
    """

    # create dict
    filtered_tweet = {
        "id": tweet["data"]["id"],
        "rules": [],
        "coordinates": [],
    }

    # append rule ids
    for rule in tweet["matching_rules"]:
        filtered_tweet["rules"].append(rule["id"])

    if (
        "coordinates" in tweet["data"]["geo"]
        and tweet["data"]["geo"]["coordinates"]["type"] == "Point"
    ):

        # append existing coordinates
        filtered_tweet["coordinates"] = tweet["data"]["geo"]["coordinates"][
            "coordinates"
        ]

    elif "places" in tweet["includes"]:

        # use center of provided bounding box of the place
        for place in tweet["includes"]["places"]:

            bbox = place["geo"]["bbox"]

            # create MultiPoint object from the bounding box coordinates
            multipoint = MultiPoint([(bbox[0], bbox[1]), (bbox[2], bbox[3])])

            # set the centroid of the bounding box as coordinates
            filtered_tweet["coordinates"] = multipoint.centroid.coords[0]

    return filtered_tweet
