import os
import requests
from pydantic import BaseModel
from typing import Optional


class Rule(BaseModel):

    value: str
    precision: int
    tag: Optional[str] = None


bearer_token = os.environ.get("TWITTER_BEARER_TOKEN")
rules_api_url = os.environ.get("TWITTER_RULES_API_URL")


def bearer_oauth(request):

    request.headers["Authorization"] = f"Bearer {bearer_token}"
    return request


def get_rules():

    return requests.get(
        rules_api_url,
        auth=bearer_oauth,
    )


def post_rule(rule):

    return requests.post(
        rules_api_url,
        auth=bearer_oauth,
        json={"add": [{"value": rule.value, "tag": rule.tag}]},
    )


def delete_rule(rule_id):

    # use POST request to _delete_ the rule according to Twitter API docs
    return requests.post(
        rules_api_url,
        auth=bearer_oauth,
        json={"delete": {"ids": [rule_id]}},
    )
