"""
Main module defines routes and implements business logic
"""

import uvicorn
from fastapi import FastAPI
from fastapi.responses import JSONResponse, HTMLResponse
from fastapi.staticfiles import StaticFiles
from sink.twitter import rules, tweets
from sink.lbsn import rule_tables, posts, cardinalities
import markdown

# create a FastAPI object
app = FastAPI()

# mount static files
app.mount("/static", StaticFiles(directory="static"), name="static")


@app.post("/posts")
async def post_twitter_post(tweet: dict):
    """
    Store a Twitter post in the local database
    """

    # filter tweet
    post = tweets.filter_tweet(tweet)

    # sort coordinates
    post = posts.sort_post_coordinates(post)

    # store post in sinkdb
    if posts.store_post(post):

        return JSONResponse(post, status_code=201)

    # return error if that fails
    return JSONResponse(
        status_code=400, content={"error": "storing post failed"}
    )


@app.delete("/rules/{rule_id}")
async def delete_twitter_rule(rule_id: str):
    """
    Delete a rule from the Twitter API filtered stream
    and the corresponding table from the local database
    """

    twitter_response = rules.delete_rule(rule_id)

    if twitter_response.json()["meta"]["summary"]["deleted"] == 1:

        # drop rule_table
        if rule_tables.drop_rule_table(rule_id):
            return twitter_response.json()

        # return error if that fails
        return JSONResponse(
            status_code=400, content={"error": "drop table failed"}
        )

    return twitter_response.json()


@app.post("/rules")
async def post_twitter_rule(rule: rules.Rule):
    """
    Create a rule in the Twitter API filtered stream
    and a corresponding table in the local database
    """

    twitter_response = rules.post_rule(rule)

    # return Created if successful
    if twitter_response.json()["meta"]["summary"]["created"] == 1:
        rule_id = twitter_response.json()["data"][0]["id"]
        rule_value = twitter_response.json()["data"][0]["value"]

        # create rule_table
        if rule_tables.create_rule_table(rule_id, rule_value, rule.precision):
            return JSONResponse(twitter_response.json(), status_code=201)

        # return error if that fails
        return JSONResponse(
            status_code=400, content={"error": "create table failed"}
        )

    # return Conflict error if duplicate
    if twitter_response.json()["errors"][0]["title"] == "DuplicateRule":
        return JSONResponse(twitter_response.json(), status_code=409)

    # return bad request otherwise
    return JSONResponse(status_code=400)


@app.get("/rules/{rule_id}")
async def get_twitter_rule(rule_id: str):
    """
    Return a list of areas and their corresponding post cardinalities
    for a given rule ID
    """
    return cardinalities.get_cardinalities(rule_id)


@app.get("/rules")
async def get_twitter_rules():
    """
    Return the list of rules stored in the Twitter API filtered stream
    """
    return rules.get_rules().json()


@app.get("/", response_class=HTMLResponse)
async def root():
    """
    Return the README in HTML
    """

    with open("README.md", "r") as readme:
        md = readme.read()
        html = markdown.markdown(md)

    return f"""<!DOCTYPE html>
        <meta charset="utf-8" />
        <meta name="color-scheme" content="light dark" />
        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>VGIsink</title>
        {html}
    """


def main():

    uvicorn.run("sink.main:app", host="0.0.0.0", port=80)


if __name__ == "__main__":
    main()
