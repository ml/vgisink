#!/usr/bin/env bash

echo "Welcome to the VGIsink Twitter Filtered Stream reader script."
echo
echo -n "Getting bearer token… "

bearer_token=$(
    sed -n '/^TWITTER_BEARER_TOKEN=/ {s///p;q;}' \
        "$(dirname "$(readlink -f "$0")")/.env"
)
[[ $bearer_token == AAAA* ]] && echo "ok" || {
    echo "Error: invalid token."
    exit 1
}

echo
echo -n "Assembling Twitter-API stream url… "

twitter_api="https://api.twitter.com/2/tweets/search/stream"
tweet_fields="tweet.fields =
    author_id,
    created_at,
    geo,id,text
"
expansions="expansions =
    geo.place_id
"
place_fields="place.fields =
    id,
    geo,
    name,
    place_type
"

stream=$(
    # remove any spaces from the string
    echo "$twitter_api?$tweet_fields&$expansions&$place_fields" |
        tr -d '[:space:]'
)

echo "ok"
echo
echo -n "Assembling Sink-API url… "

sink_api_url=$(
    # get only the value of the env var
    sed -n '/^SINK_API_URL=/ {s///p;q;}' \
        "$(dirname "$(readlink -f "$0")")/.env"
)
[[ $sink_api_url == http* ]] && echo "ok" || exit 1

echo
echo "Listening to the stream and piping it line by line to the Sink-API…"

curl -X GET --silent --header "Authorization: Bearer $bearer_token" "$stream" |
    while read -r line; do

        # ignore, if line does not start with {
        [[ "${line:0:1}" != "{" ]] && continue

        # post line to sink api
        curl -X POST "$sink_api_url"/posts \
            --silent \
            --header "Content-Type:application/json" \
            --data "$line"
        echo
    done
