FROM python:3.10-slim

WORKDIR /work

ENV PIP_ROOT_USER_ACTION=ignore
ENV PIP_NO_CACHE_DIR=1

RUN python -m pip install --upgrade pip && \
    python -m pip install poetry

COPY README.md poetry.lock pyproject.toml ./

RUN poetry config virtualenvs.in-project true && \
    poetry install -v

COPY sink sink
COPY static static

# run command in array of strings format to enable ctrl-c to stop
CMD ["poetry", "run", "uvicorn", "--host", "0.0.0.0", "--port", "80", "sink.main:app"]

EXPOSE 80
